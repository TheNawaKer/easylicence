//
//  UserModel.swift
//  EasyLicence
//
//  Created by tp14 on 21/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Classe modèle d'un carnet de route

@objc(TravelogueModel)
class TravelogueModel : NSObject,NSCoding {
    var UserId : Int?
    var NbKms : Float?
    var NbHours : Float?
    
    override init () {}
    
    init (userId : Int, nbKms : Float, nbHours : Float) {
        self.UserId = userId
        self.NbKms = nbKms
        self.NbHours = nbHours
    }
    
    init (json: [String: AnyObject]) {
        self.UserId = Int ((json["UserId"] as! String?)!)
        self.NbKms = Float ((json["Distance"] as! String?)!)
        self.NbHours = Float ((json["Hours"] as! String?)!)
    }

    required init(coder aDecoder: NSCoder){
        self.UserId = aDecoder.decodeObject(forKey: "UserId") as? Int
        self.NbKms = aDecoder.decodeObject(forKey: "NbKms") as? Float
        self.NbHours = aDecoder.decodeObject(forKey: "NbHours") as? Float
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.UserId, forKey: "UserId")
        aCoder.encode(self.NbKms, forKey: "NbKms")
        aCoder.encode(self.NbHours, forKey: "NbHours")
        
    }
    
    override var description : String  {
        return "UserId : \(UserId), Number of Kilometers : \(NbKms), Number of hours : \(NbHours)"
    }
}
