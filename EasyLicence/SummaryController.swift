//
//  SummaryController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 28.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur de la vue permettant de résumer un évènement sur le point d'être ajouté à la base de données
class SummaryController: UIViewController, EventProtocol {

    // Références vers les éléments de la vue
    @IBOutlet weak var withField: UILabel!
    @IBOutlet weak var hourField: UILabel!
    @IBOutlet weak var dateField: UILabel!
    @IBOutlet weak var addressFrom: UILabel!
    @IBOutlet weak var addressTo: UILabel!
    @IBOutlet weak var itinaryField: UILabel!
    
    // Ces variables seront remplies par la suite pour avoir accès à l'instructeur et l'évènement en question
    var instructor: UserModel? = nil
    var event: EventModel? = nil
    
    // Lors de la création de la vue, on remplie les champs
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hourField.text = (self.event?.BeginTime!)! + " - " + (self.event?.EndTime!)!
        self.withField.text = self.instructor?.getFullName()
        self.dateField.text = event?.Date
        self.addressFrom.text = event?.StartLocation
        self.addressTo.text = event?.EndLocation
        self.itinaryField.text = EventModel.itineryType[(event?.Itinary)!]
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Fonction appelée quand on clique sur le bouton de confirmation
    // L'évènement est enregistré dans la base de données
    @IBAction func doConfirm(_ sender: Any) {
        let user: UserModel = DataSingleton.sharedInstance.myUser!
        // Execution de la requête
        EventQueries.addEvent (date: (self.event?.Date!)!, beginTime: (self.event?.BeginTime!)!, endTime: (self.event?.EndTime!)!, userId: user.Id!, instructorId: (instructor?.Id!)!, startLocation: (self.event?.StartLocation)!, endLocation: (self.event?.EndLocation)!, type:(self.event?.Itinary)!, validated: false, score: 0, delegate: self)
    }
    
    // Fonction appelée lorsque l'évènement a été appelée (retour vers la vue principale)
    func onEventAdd (res: String) {
        DispatchQueue.main.async {
            if (res == "0") {
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let nextView = storyboard.instantiateViewController(withIdentifier: "Home")
                self.navigationController?.setViewControllers([nextView], animated: true)
            } else {
                print ("Add Event Error : " + res)
            }
        }
    }
}
