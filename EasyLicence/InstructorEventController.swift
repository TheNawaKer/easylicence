//
//  InstructorEventController.swift
//  EasyLicence
//
//  Created by tp13 on 30/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur de la vue permettant à l'instructeur de voir les évènements prévus
class InstructorEventController: UIViewController, UITableViewDelegate, UITableViewDataSource, EventLoadProtocol {

    // Va contenir les évènements    
    var event: [EventModel] = []

    // Référence vers la vue qui va acceuillir les évènements
    @IBOutlet weak var dataView: UITableView!

    // Quand on charge la vue, on va chercher les évènements liés à l'instructeur connecté
    override func viewDidLoad() {
        super.viewDidLoad()
        EventQueries.getEventsByInstructor(id: (DataSingleton.sharedInstance.myUser?.Id)!, delegate: self)
        dataView.delegate = self
        dataView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Quand on récupère les évènements, on met à jour la vue et notre liste d'évènements
    func onEventLoad(events: [EventModel]) {
        event = events
        DispatchQueue.main.async {
            self.dataView.reloadData()
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplvar implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return event.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellule = UITableViewCell(style: .default, reuseIdentifier: "EventCell")
        // "MaCellule" est le nom du prototype d'une cellule
        cellule.textLabel?.text = event[indexPath.row].description // ou par exemple : tableauDonnees[indexPath.row]
        return cellule
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let eventView = self.storyboard?.instantiateViewController(withIdentifier: "Event") as! EventController
        eventView.event = event[indexPath.row]
        self.navigationController?.pushViewController(eventView, animated: true)
    }
}
