//
//  EventController.swift
//  EasyLicence
//
//  Created by tp13 on 30/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controlleur permettant de gérer la vue d'un évènement
class EventController: UIViewController, UserLoadProtocol, EventUpdateProtocol {

    // Référence vers les éléments composant la vue
    @IBOutlet weak var kmsField: UITextField!
    @IBOutlet weak var scoreField: UITextField!
    @IBOutlet weak var validatedSwitch: UISwitch!
    @IBOutlet weak var dateField: UITextField!
    @IBOutlet weak var UserField: UITextField!
    @IBOutlet weak var endAdress: UITextField!
    @IBOutlet weak var itineryTypeField: UITextField!
    @IBOutlet weak var startAdress: UITextField!
    
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var beginTime: UITextField!
    @IBOutlet weak var endTime: UITextField!

    @IBOutlet weak var closeButton: UIButton!
    // Variables représentant l'évènement et l'utilisateur
    var event: EventModel? = nil
    var user: UserModel? = nil
    
    // Lors de la construction de la vue, on remplie celle ci avec les données de l'event et du user grace au singleton
    override func viewDidLoad() {
        super.viewDidLoad()

        dateField.text = event?.Date
        startAdress.text = event?.StartLocation
        endAdress.text = event?.EndLocation
        itineryTypeField.text = String((event?.Itinary!)!)
        validatedSwitch.isOn = event?.Validated! == 1

        if validatedSwitch.isOn{
            validatedSwitch.isEnabled = false
        }

        scoreField.text = String((event?.Score!)!)
        kmsField.text = String((event?.Kms!)!)
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd HH:mm"
        
        let date = (event?.Date!)! + " " + (event?.EndTime!)!
        if Date() < dateFormatter.date(from: date)! || !validatedSwitch.isOn {
            if !validatedSwitch.isOn {
                validatedSwitch.isEnabled = false
            }
            scoreField.isEnabled = false
            kmsField.isEnabled = false
        }else{
            if self.event?.Validated == 1{
                self.closeButton.isHidden = false
                scoreField.isEnabled = true
                scoreField.isEnabled = true
            }
        }
        
        // Si on est un instructeur, on a donc des étudiants dans le calendrier alors on va chercher son nom dans la base de données
        if DataSingleton.sharedInstance.myUser?.Role == 1{
            UserQueries.getUserById(id: (event?.StudentId)!, delegate: self)
        } else {
            // Si on est un étudiant on va chercher le nom de l'instructeur
            userLabel.text = "Instructor:"
            UserQueries.getUserById(id: (event?.InstructorId)!, delegate: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Fonction appelée quand on a demandé au serveur d'aller nous chercher un utilisateur en fonction de son identifiant
    func onUserDidLoad(user: UserModel) {
        DispatchQueue.main.async {
            self.UserField.text = user.getFullName()
        }
    }
    @IBAction func onValidate(_ sender: Any) {
        DispatchQueue.main.async {
            let refreshAlert = UIAlertController(title: "Warning", message: "You are about to validate the appointment, are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
                self.validatedSwitch.isEnabled = false
                EventQueries.validateEvent(eventId: (self.event?.Id)!, delegate: self)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }
    }
    
    func onEventValidate(err: Bool) {
        if (!err){
            self.scoreField.isEnabled = true
            self.kmsField.isEnabled = true
        }else{
            self.validatedSwitch.isEnabled = true
        }
    }
    @IBAction func onCloseEvent(_ sender: Any) {
        DispatchQueue.main.async {
            let refreshAlert = UIAlertController(title: "Warning", message: "You are about to close the appointment, are you sure?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { (action: UIAlertAction!) in
                self.validatedSwitch.isEnabled = false
                EventQueries.closeEvent(event: self.event!, delegate: self)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
        }

    }
    
    func onEventClose(err: Bool) {
        if !err{
            self.scoreField.isEnabled = false
            self.kmsField.isEnabled = false
            self.closeButton.isHidden = true
        }
    }
}
