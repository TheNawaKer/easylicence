//
//  EventDetailController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 30.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

class EventDetailController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    // Références vers les éléments d'un évènement dans la vue
    @IBOutlet weak var endAddress: UITextField!
    @IBOutlet weak var startAddress: UITextField!
    @IBOutlet weak var itinerySelector: UIPickerView!

    var instructor: UserModel? = nil
    var event: EventModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.itinerySelector.delegate = self
        self.itinerySelector.dataSource = self        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // The number of columns of data
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // The number of rows of data
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return EventModel.itineryType.count
    }
    
    // The data to return for the row and component (column) that's being passed in
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return EventModel.itineryType[row]
    }
    
    // Catpure the picker view selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // This method is triggered whenever the user makes a change to the picker selection.
        // The parameter named row and component represents what was selected.
    }
    
    @IBAction func goToSummary(_ sender: Any) {
        self.event?.StartLocation = startAddress.text
        self.event?.EndLocation = endAddress.text
        self.event?.Itinary = itinerySelector.selectedRow(inComponent: 0)
        let summary = self.storyboard?.instantiateViewController(withIdentifier: "Summary") as! SummaryController
        summary.instructor = instructor
        summary.event = event
        self.navigationController?.pushViewController(summary, animated: true)
    }


}
