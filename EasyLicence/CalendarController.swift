//
//  ViewController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 14.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit
import JTAppleCalendar

// Classe controleur permettant de gérer le calendrier
class CalendarController: UIViewController {
    
    let white = UIColor(colorWithHexValue: 0xECEAED)
    let darkPurple = UIColor(colorWithHexValue: 0x3A284C)
    let dimPurple = UIColor(colorWithHexValue: 0x574865)
    
    public var selectDateIsAllowed: Bool = true
    
    // Référence vers le label pour afficher le mois et vers le calendrier en lui-même
    @IBOutlet weak var monthHeader: UILabel!
    @IBOutlet weak var calendar: JTAppleCalendarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        calendar.dataSource = self
        calendar.delegate = self
        calendar.registerCellViewXib(file: "CalendarCellView") // Registering your cell is manditory
        calendar.cellInset = CGPoint(x: 0, y: 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

extension UIColor {
    convenience init(colorWithHexValue value: Int, alpha:CGFloat = 1.0){
        self.init(
            red: CGFloat((value & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((value & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(value & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}
extension CalendarController: JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        var components = DateComponents()
        components.setValue(1, for: .year)
        let date: Date = Date()
        self.updateHeaderMonth(date: date)
        let startDate = Date() // You can use date generated from a formatter
        let endDate = Calendar.current.date(byAdding: components, to: date)
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate!,
                                                 numberOfRows: 5, // Only 1, 2, 3, & 6 are allowed
            calendar: Calendar.current,
            generateInDates: .forAllMonths,
            generateOutDates: .tillEndOfRow,
            firstDayOfWeek: .monday)
        return parameters
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplayCell cell: JTAppleDayCellView, date: Date, cellState: CellState) {
        let myCustomCell = cell as! CalendarCellView
        
        // Setup Cell text
        myCustomCell.dayLabel.text = cellState.text
        
        handleCellTextColor(view: cell, cellState: cellState)
        handleCellCurrentDate(view: cell, cellState: cellState, date: date)
        if selectDateIsAllowed{
            handleCellSelection(view: cell, cellState: cellState)
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        if selectDateIsAllowed {
            handleCellSelection(view: cell, cellState: cellState)
            handleCellTextColor(view: cell, cellState: cellState)
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleDayCellView?, cellState: CellState) {
        if selectDateIsAllowed {
            handleCellSelection(view: cell, cellState: cellState)
            handleCellTextColor(view: cell, cellState: cellState)
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo){
        self.updateHeaderMonth(date: visibleDates.monthDates[0])
    }
    
    func updateHeaderMonth(date: Date){
        let dateComponents = Calendar.current.dateComponents([.month], from: date)
        self.monthHeader.text = Calendar.current.monthSymbols[dateComponents.month! - 1]
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTAppleDayCellView?, cellState: CellState) {
        
        guard let myCustomCell = view as? CalendarCellView  else {
            return
        }
        
        if cellState.isSelected {
            myCustomCell.dayLabel.textColor = darkPurple
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dayLabel.textColor = UIColor.black
            } else {
                myCustomCell.dayLabel.textColor = white
            }
        }
    }
    
    // Function to handle the calendar selection
    func handleCellSelection(view: JTAppleDayCellView?, cellState: CellState) {
        guard let myCustomCell = view as? CalendarCellView  else {
            return
        }
        if cellState.isSelected {
            myCustomCell.selectedView.layer.cornerRadius =  25
            myCustomCell.selectedView.isHidden = false
            myCustomCell.dayLabel.textColor = white
        } else {
            myCustomCell.selectedView.isHidden = true
        }
    }
    
    // Function to handle the calendar selection
    func handleCellCurrentDate(view: JTAppleDayCellView?, cellState: CellState, date: Date) {
        guard let myCustomCell = view as? CalendarCellView  else {
            return
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let actualDate = formatter.string(from: Date())
        let cellDate = formatter.string(from: date)
        
        if actualDate == cellDate{
            myCustomCell.actualDateView.isHidden = false;
            myCustomCell.actualDateView.layer.cornerRadius = 25
            myCustomCell.dayLabel.textColor = white
        }else{
            myCustomCell.actualDateView.isHidden = true
        }
    }
    
    func getSelectedDate() -> [Date] {
        return self.calendar.selectedDates
    }
    
}

