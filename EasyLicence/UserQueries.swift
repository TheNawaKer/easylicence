//
//  UserQueries.swift
//  EasyLicence
//
//  Created by TheNawaKer on 26.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Protocole utilisé pour les évènements
// Ces méthodes sont appelée en fonction de la requête effectuée
protocol UserLoginProtocol {
    func onLoginError(err: String)
    func onLoginSuccess(user: UserModel)
}

protocol UserRegisterProtocol {
    func onRegisterError(err: String)
    func onRegisterSuccess()
}

protocol UserLoadProtocol{
    func onUserDidLoad(user: UserModel)
}

protocol InstructorsLoadProtocol {
    func onInstructorsLoad(instructors: [UserModel])
}

// Classe permettant d'executer les requêtes à la base de données concernant les utilisateurs
class UserQueries{
    
    // Nom du service
    static let ServiceModel: String = "User"
    
    // Fonction permettant de connecter un utilisateur.
    //  Param email : l'email de l'utilisateur
    //  Param password : le mot de passe de l'utilisateur
    //  Param delegate : object implémentant le protocole UserLoginProtocol 
    static func doLogin(email: String, password: String, delegate: UserLoginProtocol){

        // Création des paramètres et de l'url
        let params: [String: String] = ["Email": email, "Password": password]
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "Login", params: params)

        // Execution de la requête
        BaseQueries.callUrl(newUrl: url) { (result) in
            let res = (!result.isEmpty && result.characters.first == "{")

            // Si l'authentification a échouée, on appelle la fonction appropriée pour afficher un message d'erreur
            if !res {
                delegate.onLoginError(err: result)
            } else {
                // Dans le cas contraire, on récupère les données reçues à propos de l'utilisateur et on appelle la fonction pour gérer ces données
                let userJson: [String:AnyObject] = BaseQueries.convertStringToDictionary(text: result)!
                let user = UserModel(json: userJson)
                delegate.onLoginSuccess(user: user)
            }
        }
    }
    

    // Permet d'enregister un utilisateur
    //  Param user : l'utilisateur à enregistrer
    //  Param delagate : object implémentant le protocole UserRegisterProtocol
    static func doRegister(user: UserModel, delegate: UserRegisterProtocol) {

        // Création des paramètres et de l'url
        let params: [String: String] = ["FirstName":user.FirstName!,"LastName":user.LastName!,"Email":user.Email!,"PhoneNumber":user.PhoneNumber!,"Password":user.Password!,"Role":String(user.Role!)]
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "Register", params: params)

        // Execution de la requête 
        BaseQueries.callUrl(newUrl: url) { (result) in
            let res = (result != "100") // les codes d'erreurs sont spécifiés dans les classes PHP correspondant aux services
            if !res {
                delegate.onRegisterError(err: result)
            } else {
                delegate.onRegisterSuccess()
            }
        }
    }
    
    // Permet de récupérer un utilisateur en fonction de son identifiant
    //  Param id : l'identifiant de l'utilisateur
    //  Param delegate : object implémentant le protocole UserLoadProtocol
    static func getUserById(id: Int, delegate: UserLoadProtocol) {

        // Création de l'url et des paramètres 
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "GetById", params: ["Id":String(id)])

        // Execution de la requête 
        BaseQueries.callUrl(newUrl: url) { (result) in
            if !result.isEmpty{
                // Convertion du JSON en dictionnaire puis interprétation des données dans la fonction adéquat du protocole
                let data: [String:AnyObject] = BaseQueries.convertStringToDictionary(text: result)!
                delegate.onUserDidLoad(user: UserModel(json: data))
            }
        }
    }
    
    // Permet de récupérer tous les instructeurs
    //  Param delegate : object implémentant le protocole InstructorsLoadProtocol
    static func getAllDrivingInstructor(delegate: InstructorsLoadProtocol) {

        // Création des paramètres et de la requête
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "GetInstructors", params: nil)

        // Execution de la requête
        BaseQueries.callUrl(newUrl: url) { (result) in
            if(!result.isEmpty) {
                // Création de la liste d'instructeurs en fonction de résultat JSON
                let data: [[String: AnyObject]]? = BaseQueries.convertStringToArrayDictionary(text: result)
                var instructors: [UserModel] = []
                for user in data!{
                    instructors.append(UserModel(json: user))
                }
                delegate.onInstructorsLoad(instructors: instructors)
            }
        }
    }
}
