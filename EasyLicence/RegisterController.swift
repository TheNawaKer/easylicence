//
//  ViewController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 14.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur permettant de gérer la vue d'inscription
class RegisterController: UIViewController, UserRegisterProtocol {
    
    // références vers les différents  champs de la vue
    @IBOutlet weak var FirstNameField: UITextField!
    @IBOutlet weak var LastNameField: UITextField!
    @IBOutlet weak var EmailField: UITextField!
    @IBOutlet weak var PhoneNumberField: UITextField!
    @IBOutlet weak var PasswordField: UITextField!
    @IBOutlet weak var roleSegment: UISegmentedControl! // permet à l'utilisateur de choisir son statu Etudiant/Moniteur
    
    // Fonction appelée quand on clique sur le bouton d'inscription
    // Si les données sont correctes (tous les champs sont remplies, l'email est correct), on enregistre l'utilisateur
    @IBAction func onClickRegister(_ sender: UIButton) {
        if (!(FirstNameField.text?.isEmpty)! && !(LastNameField.text?.isEmpty)! && !(EmailField.text?.isEmpty)! && !(PhoneNumberField.text?.isEmpty)! && !(PasswordField.text?.isEmpty)!) {
            if (self.isValidEmail(address: EmailField.text!)) {
                let user: UserModel = UserModel()
                user.FirstName = FirstNameField.text
                user.LastName = LastNameField.text
                user.Password = PasswordField.text
                user.Email = EmailField.text
                user.PhoneNumber = PhoneNumberField.text
                if roleSegment.selectedSegmentIndex == 0 {
                    user.Role = 2
                } else {
                    user.Role = 1
                }
                // Effectue la requête d'inscription
                UserQueries.doRegister(user: user, delegate: self)
            } else {
                // L'email est incorrect, message d'erreur
                DispatchQueue.main.async {
                    let alert = UIAlertController(title: "Error", message: "Please enter a correct email address.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        } else {
            // Tous les champs ne sont pas remplies, message d'erreur
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: "Some field are empty !", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    // Permet de retourner à la vue précédente (Login)
    func goBack () {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Fonction appelée si le serveur a refusée l'inscription (message d'erreur)
    func onRegisterError(err: String) {
        DispatchQueue.main.async { // On demande a la vue de se mettre à jour => Gestionnaire de thread transmet l'event a la vue
            let alert = UIAlertController(title: "Alert", message: "This email address is already used !", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Click", style: UIAlertActionStyle.default, handler:nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    // Fonction appelée en cas de succès lors de l'inscription (retour vers page de Login)
    func onRegisterSuccess() {
        DispatchQueue.main.async {
            self.goBack()
        }
    }

    // Fonction permettant de vérifier si une adresse email est correcte
    //  Param address : l'adresse à vérifier
    func isValidEmail (address: String) -> Bool {
        let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let test = NSPredicate(format: "SELF MATCHES %@", regEx)
        return test.evaluate(with: address)
    }}

