//
//  ScoreQueries.swift
//  EasyLicence
//
//  Created by tp14 on 30/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Protocol utilisé pour les scores
protocol ScoreProtocol {
    func onScoreAdd (res: String)
    func onScoreRecv (res: ScoreModel)
    func onScoresRecv (res: [ScoreModel])
}

// Classe permettant d'executer des requetes sur les scores
class ScoreQueries {
    
    // Nom du service
    static let serviceModel: String = "Score"
    
    // Permet de récupérer les scores d'un etudiant
    static func getStudentScoresById (id : Int, delegate: ScoreProtocol) {
        
        // Création des paramètres et de l'url
        let params: [String: String] = ["StudentId": String (id)]
        let url = BaseQueries.makeUrl(service: serviceModel, action: "GetByStudentId", params: params)
        
        // Execution de la requete
        BaseQueries.callUrl (newUrl: url) { (result) in
            if(!result.isEmpty) {
                let data: [[String: AnyObject]]? = BaseQueries.convertStringToArrayDictionary(text: result)
                var scores: [ScoreModel] = []
                for score in data!{
                    scores.append(ScoreModel(json: score))
                }
                delegate.onScoresRecv(res: scores)
            }
        }
    }
    
    // Permet de récupérer les scores donné par un instructor
    static func getInstructorScoresById (id : Int, delegate: ScoreProtocol) {
        
        // Création des paramètres et de l'url
        let params: [String: String] = ["InstructorId": String (id)]
        let url = BaseQueries.makeUrl(service: serviceModel, action: "GetByInstructorId", params: params)
        
        // Execution de la requete
        BaseQueries.callUrl (newUrl: url) { (result) in
            if(!result.isEmpty) {
                let data: [[String: AnyObject]]? = BaseQueries.convertStringToArrayDictionary(text: result)
                var scores: [ScoreModel] = []
                for score in data!{
                    scores.append(ScoreModel(json: score))
                }
                delegate.onScoresRecv(res: scores)
            }
        }
    }
    
    // Permet d'ajouter un score
    static func addScore (studentId: Int, instructorId: Int, score: Float, delegate: ScoreProtocol) {
        
        // Creation des paramètres et de l'url
        let params: [String: String] = ["StudentId" : String (studentId), "InstructorId": String (instructorId), "Score": String (score)]
        let url = BaseQueries.makeUrl (service: serviceModel, action: "Add", params: params)
        
        // Execution de la requete
        BaseQueries.callUrl (newUrl: url) { (result) in
            delegate.onScoreAdd (res: result)
        }
    }
}
