//
//  InstructorViewController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 28.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur permettant de gérer la vue affichant la liste des moniteurs pour l'étudant
class InstructorViewController: UITableViewController, InstructorsLoadProtocol {    

    // Va contenir la liste des moniteurs récupérés
    var instructors: [UserModel] = []

    // Quand on charge la vue, on va chercher la liste des moniteurs
    override func viewDidLoad() {
        super.viewDidLoad()
        UserQueries.getAllDrivingInstructor(delegate: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // Quand on recoit les données, on met à jour la vue et le tableau de moniteurs    
    func onInstructorsLoad(instructors: [UserModel]) {
        self.instructors = instructors
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }        
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return instructors.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellule = UITableViewCell(style: .default, reuseIdentifier: "InstructorCell")
        // "MaCellule" est le nom du prototype d'une cellule
        cellule.textLabel?.text = instructors[indexPath.row].FirstName // ou par exemple : tableauDonnees[indexPath.row]
        return cellule
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //self.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let appointment = self.storyboard?.instantiateViewController(withIdentifier: "Appointment") as! AppointmentController
        appointment.instructor = instructors[indexPath.row]
        self.navigationController?.pushViewController(appointment, animated: true)
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
