//
//  TravelogueController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 25.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation
import UIKit

// Class controleur permettant de gérer le carnet de route
class TravelogueController: UIViewController, URLSessionDataDelegate, TravelogueProtocol {
    
    // références vers les élements de la vue
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var nbKms: UITextField!
    @IBOutlet weak var nbHours: UITextField!
    
    // cette variable sera rempli avec l'utilisateur connecté
    var user: UserModel? = nil

    // Quand la vue est chargée, on renseigne les champs de la vue
    override func viewDidLoad() {
        super.viewDidLoad()
        user = DataSingleton.sharedInstance.myUser!
        lastName.text = user?.LastName
        firstName.text = user?.FirstName
        email.text = user?.Email
        phoneNumber.text = user?.PhoneNumber
        
        // On récupère le carnet de route de l'utilisateur dans la base de données
        TravelogueQueries.getTravelogueByUserId(id: (user?.Id)!, delegate: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Fonction appelée si aucun carnet de route n'est trouvé dans la base
    func onTravelogueError (err: String) {
        // Si l'utilisateur n'a pas de carnet de route, on en créé un
        if ((user?.Role)! > 0) {
            DispatchQueue.main.async {
                self.nbKms.text = "0Kms"
                self.nbHours.text = "0h"
            }
            TravelogueQueries.addTravelogue(id: (user?.Id)!, distance: 0, hours: 0, delegate: self)
        } else {
            print ("OnTravelogueError : " + err)
        }
    }
    
    // Fonction appelé si un carnet de route est trouvé, on met à jour les données de la vue
    func onTravelogueRecv (res: TravelogueModel) {
        print ("travelogue : " + res.description)
        DispatchQueue.main.async {
            self.nbKms.text = (res.NbKms?.description)! + "Kms"
            self.nbHours.text = (res.NbHours?.description)! + "h"
        }
    }

    // Fonction appelée quand on a créé un carnet de route
    func onTravelogueAdd (res: String) {
        if (res != "0") {
            print ("OnTravelogueAdd Error : ")
            if (res == "300") {
                print ("CARNET_ERROR_PARAM_MISSING")
            } else if (res == "302") {
                print ("CARNET_ERROR_WRITE")
            } else {
                print ("Unknown code : " + res)
            }
        } else {
            print ("Travelogue created !")
        }
    }
    
}
