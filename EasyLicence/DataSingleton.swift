//
//  DataSingleton.swift
//  EasyLicence
//
//  Created by TheNawaKer on 25.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation
import UIKit

struct Keys{
    static let user = "user"
}

// Singleton permettant de stocker l'utilisateur connecté et pour y avoir accès si nécessaire
class DataSingleton{
    
    static let sharedInstance = DataSingleton()
    
    public var myUser: UserModel?
    
    var goToBackgroundObserver: AnyObject?
    
    init(){
        let defaults = UserDefaults.standard
        
        if let data = defaults.object(forKey: Keys.user) as? NSData{
            do{
                myUser = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as! UserModel?
            }
            catch{
            }
        }
        
        goToBackgroundObserver = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: nil, using: { (note) in
            let defaults = UserDefaults.standard
            defaults.set(NSKeyedArchiver.archivedData(withRootObject: self.myUser!), forKey: Keys.user)
        })
    }
}
