//
//  AppointmentController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 28.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controlleur permettant de gérer une prise de rendez-vous (participe à la création d'un évènement)
class AppointmentController: CalendarController {
    
    // On retrouve nos datePicker permettant de choisir l'heure de début et de fin
    @IBOutlet weak var beginTime: UIDatePicker!
    @IBOutlet weak var endTime: UIDatePicker!
    // Permet de récupérer l'instructeur choisi précédemment dans le processus de création de l'evenement
    var instructor: UserModel?
    // Permet de stocker la date choisie
    private var calendarValue = Calendar.autoupdatingCurrent

    override func viewDidLoad() {
        super.viewDidLoad()

        // On initialise les champs d'heure avec celle actuelle
        self.beginTime.datePickerMode = .time
        self.endTime.datePickerMode = .time
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // Fonction appelée quand on clique sur le bouton permettant de passer à l'étape suivante de la création de l'event
    @IBAction func goToEventDetails(_ sender: Any) {

        // On vérifie sur une date a bien été selectionnée, si non on affiche une alerte
        if self.getSelectedDate() == [] {
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: "Please select a date", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
                self.present(alert, animated: true, completion: nil)
            }
        }else if beginTime.date >= endTime.date{
            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Error", message: "Start time exceeds or equal end time !", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            // On récupère les données entrées et on les formates
            let dateFormatter: DateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm"
            let beginValue: String = dateFormatter.string(from: self.beginTime.date)
            let endValue: String = dateFormatter.string(from: self.endTime.date)
            
            dateFormatter.dateFormat = "YYYY-MM-dd"
            let date = self.getSelectedDate()[0]
            let dateValue: String = dateFormatter.string(from: date)

            // On créé notre event suivant les données récupérées (pas encore stocké dans la base !)
            let event: EventModel = EventModel()
            event.Date = dateValue
            event.BeginTime = beginValue
            event.EndTime = endValue

            // Puis on passe à la dernière étape
            let eventDetail = self.storyboard?.instantiateViewController(withIdentifier: "EventDetail") as! EventDetailController
            eventDetail.instructor = instructor
            eventDetail.event = event
            self.navigationController?.pushViewController(eventDetail, animated: true)
        }
    }
}
