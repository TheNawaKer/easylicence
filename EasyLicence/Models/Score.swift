//
//  Score.swift
//  EasyLicence
//
//  Created by tp14 on 30/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Classe représentant un score

@objc(ScoreModel)
class ScoreModel : NSObject, NSCoding {
    
    var StudentId : Int?
    var InstructorId : Int?
    var Score : Float?
    
    init (studentId : Int, instructorId : Int, score : Float) {
        self.StudentId = studentId
        self.InstructorId = instructorId
        self.Score = score
    }
    
    init (json:[String:AnyObject]) {
        self.StudentId = Int((json["StudentId"] as! String?)!)
        self.InstructorId = Int((json["InstructorId"] as! String?)!)
        self.Score = Float((json["Score"] as! String?)!)
    }
    
    required init (coder aDecoder : NSCoder) {
        self.StudentId = aDecoder.decodeObject (forKey: "StudentId") as? Int
        self.InstructorId = aDecoder.decodeObject (forKey: "InstructorId") as? Int
        self.Score = aDecoder.decodeObject (forKey: "Score") as? Float
    }
    
    func encode (with aCoder: NSCoder) {
        aCoder.encode (self.StudentId, forKey: "StudentId")
        aCoder.encode (self.InstructorId, forKey: "InstructorId")
        aCoder.encode (self.Score, forKey: "Score")
    }
    
    override var description : String {
        return "StudentId : \(StudentId), InstructorId : \(InstructorId), Score : \(Score)"
    }
    
}
