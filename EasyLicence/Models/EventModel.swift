//
//  EventModel.swift
//  EasyLicence
//
//  Created by tp14 on 21/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Classe modèle représentant un évènement

@objc(EventModel)
class EventModel : NSObject, NSCoding {
    static let itineryType = ["Highway","Departmental","City"]
    var Id: Int?
    var Date : String?
    var BeginTime : String?
    var EndTime : String?
    var StudentId : Int?
    var InstructorId : Int?
    var StartLocation : String?
    var EndLocation: String?
    var Itinary : Int?
    var Validated : Int?
    var Score : Float?
    var Kms: Float?

    override init () {}

    // Contructeur classique permettant de renseigner tous les champs du modèle
    init (id: Int, date : String,beginTime: String, endTime: String, studentId : Int, instructorId : Int, startLocation : String, endLocation : String, type : Int, validated : Int, score : Float, kms: Float) {
        self.Id = id
        self.Date = date
        self.BeginTime = beginTime
        self.EndTime = endTime
        self.StudentId = studentId
        self.InstructorId = instructorId
        self.StartLocation = startLocation
        self.EndLocation = endLocation
        self.Itinary = type
        self.Validated = validated
        self.Score = score
        self.Kms = kms
    }
    
    // Constructeur prenant un dictionnaire en paramètre
    init(json: [String:AnyObject]){
        self.Id = Int((json["Id"] as! String?)!)
        self.Date = (json["Date"] as! String?)
        self.BeginTime = json["BeginTime"] as! String?
        self.EndTime = json["EndTime"] as! String?
        self.StudentId = Int((json["StudentId"] as! String?)!)
        self.InstructorId = Int((json["InstructorId"] as! String?)!)
        self.StartLocation = json["StartLocation"] as! String?
        self.EndLocation = json["EndLocation"] as! String?
        self.Itinary = Int((json["Type"] as! String?)!)
        self.Validated = Int((json["Validated"] as! String?)!)
        self.Score = Float((json["Score"] as! String?)!)
        self.Kms = Float((json["Kms"] as! String?)!)
    }
    
    
    // Gère la désérialisation
    required init (coder aDecoder : NSCoder) {
        self.Id = aDecoder.decodeObject (forKey: "Id") as? Int
        self.Date = aDecoder.decodeObject (forKey: "Date") as? String
        self.BeginTime = aDecoder.decodeObject (forKey: "BeginTime") as? String
        self.EndTime = aDecoder.decodeObject (forKey: "EndTime") as? String
        self.StudentId = aDecoder.decodeObject (forKey: "StudentId") as? Int
        self.InstructorId = aDecoder.decodeObject (forKey: "InstructorId") as? Int
        self.StartLocation = aDecoder.decodeObject (forKey: "StartLocation") as? String
        self.EndLocation = aDecoder.decodeObject (forKey: "EndLocation") as? String
        self.Itinary = aDecoder.decodeObject (forKey: "Itinary") as? Int
        self.Validated = aDecoder.decodeObject (forKey: "Validated") as? Int
        self.Score = aDecoder.decodeObject (forKey: "Score") as? Float
        self.Kms = aDecoder.decodeObject (forKey: "Kms") as? Float
    }

    // gère la sérialisation de l'objet
    func encode (with aCoder: NSCoder) {
        aCoder.encode (self.Id, forKey: "Id")
        aCoder.encode (self.Date, forKey: "Date")
        aCoder.encode (self.BeginTime, forKey: "BeginTime")
        aCoder.encode (self.EndTime, forKey: "EndTime")
        aCoder.encode (self.StudentId, forKey: "StudentId")
        aCoder.encode (self.InstructorId, forKey: "InstructorId")
        aCoder.encode (self.StartLocation, forKey: "StartLocation")
        aCoder.encode (self.EndLocation, forKey: "EndLocation")
        aCoder.encode (self.Itinary, forKey: "Itinary")
        aCoder.encode (self.Validated, forKey: "Validated")
        aCoder.encode (self.Score, forKey: "Score")
        aCoder.encode (self.Kms, forKey: "Kms")
    }

    override var description : String {
        return "Id: \(Id), Date : \(Date),BeginTime : \(BeginTime),EndTime : \(EndTime), StudentId : \(StudentId), InstructorId : \(InstructorId), StartLocation : \(StartLocation), EndLocation : \(EndLocation), Type : \(Itinary), Validated : \(Validated), Score : \(Score), Kms : \(Kms)"
    }
}
