//
//  UserModel.swift
//  EasyLicence
//
//  Created by tp14 on 21/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Classe modèle représentant un utilisateur

@objc(UserModel)
class UserModel : NSObject,NSCoding {
    var Id : Int?
    var Email : String?
    var LastName : String?
    var FirstName : String?
    var Password : String?
    var PhoneNumber : String?
    var Role : Int?
    
    override init () {}
    
    // Constructeur classique
    init (id : Int, lastName : String, firstName : String, email : String, password : String, phoneNumber : String, role : Int) {
        self.Id = id
        self.Email = email
        self.LastName = lastName
        self.FirstName = firstName
        self.Password = password
        self.PhoneNumber = phoneNumber
        self.Role = role
    }
    
    // Constructeur via un dictionnaire
    init(json: [String:AnyObject]){
        self.Id = Int((json["Id"] as! String?)!)
        self.Email = json["Email"] as! String?
        self.FirstName = json["FirstName"] as! String?
        self.LastName = json["LastName"] as! String?
        self.Password = json["Password"] as! String?
        self.PhoneNumber = json["PhoneNumber"] as! String?
        self.Role = Int((json["Role"] as! String?)!)
    }
    
    required init(coder aDecoder: NSCoder){
        self.Id = aDecoder.decodeObject(forKey: "Id") as? Int
        self.Email = aDecoder.decodeObject(forKey: "Email") as? String
        self.FirstName = aDecoder.decodeObject(forKey: "FirstName") as? String
        self.LastName = aDecoder.decodeObject(forKey: "LastName") as? String
        self.Password = aDecoder.decodeObject(forKey: "Password") as? String
        self.PhoneNumber = aDecoder.decodeObject(forKey: "PhoneNumber") as? String
        self.Role = aDecoder.decodeObject(forKey: "Role") as? Int
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.Id, forKey: "Id")
        aCoder.encode(self.Email, forKey: "Email")
        aCoder.encode(self.FirstName, forKey: "FirstName")
        aCoder.encode(self.LastName, forKey: "LastName")
        aCoder.encode(self.Password, forKey: "Password")
        aCoder.encode(self.PhoneNumber, forKey: "PhoneNumber")
        aCoder.encode(self.Role, forKey: "Role")
        
    }
    
    func getFullName() -> String{
        return self.FirstName! + " " + self.LastName!
    }
    
    override var description : String  {
        return "Id : \(Id), First Name : \(FirstName), Last Name : \(LastName)"
    }
}
