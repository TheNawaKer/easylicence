//
//  CellView.swift
//  EasyLicence
//
//  Created by tp13 on 19/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation
import JTAppleCalendar

// Représente une cellule de calendrier
class CalendarCellView: JTAppleDayCellView{
    
    @IBOutlet var actualDateView: UIView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var selectedView: UIView!
}
