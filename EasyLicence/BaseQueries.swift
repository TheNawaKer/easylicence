//
//  BaseQueries.swift
//  EasyLicence
//
//  Created by TheNawaKer on 26.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Class de base à toutes les requêtes. Permet de construire des requêtes en générant l'url à partir de celle de base et de paramètres
class BaseQueries{

    // Url de base
    public static var ServiceUrl: String = "https://guillaume.gas-ntic.fr/easylicence/index.php?"
    
    // Permet d'executer la requête avec l'url passée en paramètre 
    //  Param newUrl : l'url à utiliser
    //  Param delegate : la fonction à appeler avec le résultat obtenu
    static func callUrl(newUrl: String, delegate: @escaping (String) -> Void)-> Void{

        // Création de l'url et de la session d'url permettant d'executer la requête avec une configuration par défaut
        let url: URL = URL (string: newUrl)!
        var session: URLSession!
        let configuration = URLSessionConfiguration.default
        
        session = URLSession (configuration: configuration)
        
        // Execution de la requête et appel de la fonction passée en paramètre
        let task = session.dataTask(with: url, completionHandler: {(data, response, error) -> Void in
            if let urlcontent = data {
                let result = String(data: urlcontent, encoding: String.Encoding.utf8)!
                print("\(newUrl) : \(result)")
                delegate(result)
            }
        })
        task.resume()
    }
    
    // Permet de générer une url avec un service et des paramètres données
    //  Param service : le service
    //  Param params : dictionnaire des paramètres à placer dans l'url
    static func makeUrl(service: String, action: String, params: [String: String]?) -> String{
        var url = ServiceUrl+"service="+service+"&action="+action;
        if params != nil {
            for param in params!{
                url += "&"+param.key+"="+param.value
            }
        }
        return url
    }
    
    // Permet de convertir notre JSON recu du serveur en dictionnaire
    static func convertStringToDictionary (text: String) -> [String:AnyObject]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    // Permet de convertir notre JSON recu du serveur en tableau de dictionnaires
    static func convertStringToArrayDictionary (text: String) -> [[String:AnyObject]]? {
        if let data = text.data(using: String.Encoding.utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String:AnyObject]]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
    
    
}
