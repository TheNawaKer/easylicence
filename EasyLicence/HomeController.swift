//
//  HomeController.swift
//  EasyLicence
//
//  Created by TheNawaKer on 28.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur de la première vue rencontrée après la connection, permet de gérer cette dernière
class HomeController: CalendarController {

    // Référence vers le bouton principale, ce dernier est amené à changer suivant le status de l'utilisateur connecté
    @IBOutlet weak var MainButton: UIButton!

    // L'utilisateur connecté va être renseigné ici
    var User: UserModel? = nil

    // Quand on charge la vue, on va chercher l'utilisateur connecté dans le singleton
    override func viewDidLoad() {
        super.viewDidLoad()

        self.User = DataSingleton.sharedInstance.myUser!

        // Si c'est un instructeur, on change le titre du bouton
        if (self.User?.Role == 1) {
            MainButton.setTitle ("My Events", for: .normal)
            
        }
        self.selectDateIsAllowed = false
    }

    // Fonction appelée quand on appuie sur le bouton principale
    // Ouvre une vue différente selon si l'utilisateur connecté est un étudiant ou un instructeur
    @IBAction func onClickMainButton(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if (self.User?.Role == 1) {
            let nextView = storyboard.instantiateViewController(withIdentifier: "InstructorEvents")
            self.navigationController?.pushViewController(nextView, animated: true)
        } else {
            let nextView = storyboard.instantiateViewController(withIdentifier: "InstructorListView")
            self.navigationController?.pushViewController(nextView, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
