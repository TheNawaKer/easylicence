//
//  TravelogueQueries.swift
//  EasyLicence
//
//  Created by TheNawaKer on 27.03.17.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Protocole utilisé pour le carnet de route
// Ces méthodes sont appelée en fonction de la requête effectuée
protocol TravelogueProtocol {
    func onTravelogueError (err: String)
    func onTravelogueRecv (res: TravelogueModel)
    func onTravelogueAdd (res: String)
}

// Classe permettant permettant d'executer des requêtes sur le carnet du route
class TravelogueQueries{
    
    //Nom du service
    static let ServiceModel: String = "Carnet"

    //Permet de récupérer le carnet de route d'un étudiant
    // param id : l'id de l'utilisateur
    // param delegate : objet implémentant le protocole TravelogueProtocol
    static func getTravelogueByUserId(id: Int, delegate: TravelogueProtocol) {

        // Création des paramètres de la requête et génération de l'url
        let params: [String: String] = ["Id": String (id)]
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "GetByUserId", params: params)

        // Execution de la requête 
        BaseQueries.callUrl (newUrl: url) { (result) in 
            let res = (!result.isEmpty && result.characters.first == "{")

            // Dans le cas d'une erreur on appelle la fonction correspondante du protocole
            if !res {
                delegate.onTravelogueError(err: result)
            } else {
                // Dans le cas contraire, on récupère la réponse au format JSON, on converti le résultat pour passer le dictionnaire 
                // à la fonction prévue dans le protocole pour gérer les données recues
                let travelJson: [String: AnyObject] = BaseQueries.convertStringToDictionary(text: result)!
                let travelogue = TravelogueModel(json: travelJson)
                delegate.onTravelogueRecv(res: travelogue)
            }
        }
    }

    // Permet de créer un carnet de route 
    //  Param id : l'id de l'utilisateur
    //  Param distance : la distance parcourue
    //  Param hours : le nombre d'heure
    //  Param delegate : object implémentant le protocole TravelogueProtocol
    static func addTravelogue(id: Int, distance: Float, hours: Float, delegate: TravelogueProtocol) {

        // Création des paramètres de la requête et génération de l'url
        let params: [String: String] = ["UserId": String (id), "Distance": String (distance), "Hours": String (hours)]
        let url = BaseQueries.makeUrl (service: ServiceModel, action: "Add", params: params)

        // Execution de la requête et appel de la fonction du protocole correspondante à la requête 
        BaseQueries.callUrl (newUrl: url) { (result) in 
            delegate.onTravelogueAdd (res: result)
        }
    }
}
