//
//  EventQueries.swift
//  EasyLicence
//
//  Created by tp14 on 30/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import Foundation

// Protocoles utilisé pour les évènements
// Ces méthodes sont appelée en fonction de la requête effectuée
protocol EventProtocol {
    func onEventAdd (res: String)
}

protocol EventUpdateProtocol {
    func onEventValidate(err: Bool)
    func onEventClose(err: Bool)
}

protocol EventLoadProtocol {
    func onEventLoad (events: [EventModel])
}

// Classe permettant permettant d'executer des requêtes sur les évènements
class EventQueries {
    
    // Le nom du service
    static let ServiceModel: String = "Event"
    
    // Permet d'ajouter une évènement
    //  Param date : la date de l'event au format YYYY-DD-MM
    //  Param beginTime : heure de début au format HH:MM:SS 
    //  Param endTime : heure de fin
    //  Param userId : Id de l'étudiant
    //  Param instructorId : Id de l'instructeur
    //  Param startLocation : lieu de départ
    //  Param endLocation : lieu d'arrivé
    //  Param Type : type de trajet
    //  Param Validated : boolean indiquant si le trajet a été effectué
    //  Param Score : score attribué par l'instructeur à l'étudiant
    //  Param delegate : object implémentant le protocole EventProtocol
    static func addEvent (date: String, beginTime: String, endTime: String, userId: Int, instructorId: Int, startLocation: String, endLocation: String, type: Int, validated: Bool, score: Float, delegate: EventProtocol) {
        
        // Création des paramètres et de l'url
        let params: [String: String] = ["Date": date, "BeginTime": beginTime, "EndTime": endTime, "StudentId": String (userId), "InstructorId": String (instructorId), "StartLocation": startLocation, "EndLocation": endLocation, "Type": String (type), "Validated": String (validated), "Score": String (score)]
        let url = BaseQueries.makeUrl (service: ServiceModel, action: "Add", params: params)
        
        // Execution de la requête 
        BaseQueries.callUrl (newUrl: url) { (result) in
            delegate.onEventAdd (res: result)
        }
    }
    
    // Permet de récupérer les évènements d'un moniteur
    //  Param id : l'id du moniteur
    //  Param delegate : object implémentant le protocole EventLoadProtocol
    static func getEventsByInstructor(id: Int, delegate: EventLoadProtocol) {

        // Création de l'url
        let url = BaseQueries.makeUrl(service: ServiceModel, action: "GetAllByInstructorId", params: ["InstructorId":String(id)])

        // Execution de la requête
        BaseQueries.callUrl(newUrl: url) { (result) in
            // Si la réponse n'est pas vide, on récupère le JSON, on le transforme en un tableau qu'on envoie dans 
            //  le fonction appropriée pour traiter les données
            if(!result.isEmpty) {
                let data: [[String: AnyObject]]? = BaseQueries.convertStringToArrayDictionary(text: result)
                var events: [EventModel] = []
                for user in data!{
                    events.append(EventModel(json: user))
                }
                delegate.onEventLoad(events: events)
            }
        }
    }
    
    static func validateEvent (eventId: Int, delegate: EventUpdateProtocol) {
        
        // Création des paramètres et de l'url
        let params: [String: String] = ["Id": String(eventId), "Value": "1"]
        let url = BaseQueries.makeUrl (service: ServiceModel, action: "Validate", params: params)
        
        // Execution de la requête
        BaseQueries.callUrl (newUrl: url) { (result) in
            delegate.onEventValidate(err: result == "0")
        }
    }
    
    static func closeEvent (event: EventModel, delegate: EventUpdateProtocol) {
        
        // Création des paramètres et de l'url
        let params: [String: String] = ["Id": String(event.Id!), "Kms": String(event.Kms!), "Score": String(event.Score!)]
        let url = BaseQueries.makeUrl (service: ServiceModel, action: "SetAction", params: params)
        
        // Execution de la requête
        BaseQueries.callUrl (newUrl: url) { (result) in
            delegate.onEventClose(err: result == "0")
        }
    }
}
