//
//  LoginController.swift
//  EasyLicence
//
//  Created by tp14 on 23/03/2017.
//  Copyright © 2017 inis. All rights reserved.
//

import UIKit

// Classe controleur permettant de gérer la vue de connection
class LoginController: UIViewController, URLSessionDataDelegate, UserLoginProtocol {
    
    // référence vers les deux champs de la vue
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var emailField: UITextField!

    // Fonction permettant de quitter la vue connection et d'afficher la vue principale
    func goHome () {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nextView = storyBoard.instantiateViewController(withIdentifier: "Home")
        self.navigationController?.setViewControllers([nextView], animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Fonction appelée si la connection a échouée, un message d'erreur s'affiche
    func onLoginError(err: String) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error", message: err, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler:nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Fonction appelée si la connection est un succès, on affiche la page principale "Home"
    func onLoginSuccess(user: UserModel) {
        DispatchQueue.main.async {
            DataSingleton.sharedInstance.myUser = user
            self.goHome()
        }
    }
    
    // Fonction appelée quand on clique sur le bouton de connection, demande à la base de données si le couple email/password est correct
    @IBAction func OnClick(_ sender: Any) {
        UserQueries.doLogin(email: self.emailField.text!, password: self.passwordField.text!, delegate: self)
    }
    
}

